package com.meghan.accountserviceresourceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountServiceResourceServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountServiceResourceServerApplication.class, args);
	}

}
